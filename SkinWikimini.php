<?php
/**
 * Wikimini - Modern version of MonoBook with fresh look and many usability
 * improvements.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Skins
 */

/**
 * SkinTemplate class for Wikimini skin
 * @ingroup Skins
 */
class SkinWikimini extends SkinTemplate {
	public $skinname = 'wikimini';
	public $stylename = 'Wikimini';
	public $template = 'WikiminiTemplate';

	/**
	 * Initializes output page and sets up skin-specific parameters
	 * @param OutputPage $out Object to initialize
	 */
	public function initPage( OutputPage $out ) {
		parent::initPage( $out );

		$out->addMeta( 'viewport', 'width=device-width, initial-scale=1.0' );

		// @todo For consistency with the JS file wikimini.js, this could instead be using the content language code
		// (e.g. MediaWiki\MediaWikiServices::getInstance()->getContentLanguage()->getCode()) instead of the
		// custom define().
		if ( defined( 'WIKIMINI_PROJECT_UID' ) && WIKIMINI_PROJECT_UID === 'sv' ) {
			$out->addModules( [ 'swiffy.sv.js' ] );
		} else {
			$out->addModules( [ 'swiffy.fr.js' ] );
		}

		$out->addModules( [
			'skins.wikimini.js',
			'skins.wikimini.js2'
		] );
	}

}
