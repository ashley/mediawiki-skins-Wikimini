hs.lang = {
   cssDirection:     'ltr',
   loadingText :     'Chargement...',
   loadingTitle :    'Cliquer pour annuler',
   focusTitle :      'Cliquer pour amener au premier plan',
   fullExpandTitle : 'Afficher la taille réelle',
   fullExpandText :  'Taille réelle',
   creditsText :     'D�velopp� par <i>Highslide JS</i>',
   creditsTitle :    'Site Web de Highslide JS',
   previousText :    'Précédente',
   previousTitle :   'Précédente (flèche gauche)',
   nextText :        'Suivante',
   nextTitle :       'Suivante (flèche droite)',
   moveTitle :       'Déplacer', 
   moveText :        'Déplacer',
   closeText :       'Fermer',
   closeTitle :      'Fermer (Esc)',
   resizeTitle :     'Rétablir la taille',
   playText :        'Jouer',
   playTitle :       'Lancer le diaporama (barre d\'espace)',
   pauseText :       'Pause',
   pauseTitle :      'Interrompre le diaporama (barre d\'espace)',
   number :          'Image %1/%2',
   restoreTitle :    'Cliquer pour ferme l\'image ; cliquer et glisser pour déplacer ; utiliser les touches fléchées du clavier pour avancer et reculer.'
};

hs.showCredits = false;
hs.headingEval = 'this.thumb.alt'
hs.addSlideshow({
	// slideshowGroup: 'group1',
	interval: 5000,
	repeat: false,
	useControls: true,
	fixedControls: true,
	overlayOptions: {
		opacity: .6,
		position: 'top center',
		hideOnMouseOut: true
	}
});
// Optional: a crossfade transition looks good with the slideshow
hs.transitions = ['expand', 'crossfade'];