// --------------------------------------
// YOUR Alexandria's Configuration file
// --------------------------------------

// your KEY (between quotes, e.g. : '1111-1111-1111')
var WM_Alexandria_key = '';

// URL of your CSS to be applied on Alexandria's data
// Only available with the "CSS option"
// e.g. : 'http://www.yoursite.com/alexandria/your-alexandria-data.css'
var WM_Alexandria_cssURL = null;

// Colors of the title bar
// Only available with the "CSS option"
// e.g. : 'Red', '#0083AD' (see CSS color tag values description)
var WM_Alexandria_titleBackgroundColor = null;
var WM_Alexandria_titleTextColor       = null;

// Sizes of the frames  
// Only available with the "FRAMES option"
var WM_Alexandria_topZoneHeight    = 0;
var WM_Alexandria_bottomZoneHeight = 0;
var WM_Alexandria_middleZoneHeight = 0;
var WM_Alexandria_leftZoneWidth    = 0;
var WM_Alexandria_rightZoneWidth   = 0;
var WM_Alexandria_dataWidth        = 0;

// Doctype ( null, "HTML4", "XHTML" )
// The doctype you use in your HTML pages, null if you don't use any 
var WM_Alexandria_doctype = null;

// Reserved word
// when someones clicks on these words, the page with the given url is displayed instead of Alexandria's data
// Beware : Case-sensitive !!!
// Available with all options
var WM_Alexandria_reservedWords = ["MEMODATA",               "http://www.memodata.com/alexandria-memodata/definitions_fr/def.memodata.fr.html", 
                                   "Alexandria",             "http://www.memodata.com/alexandria-memodata/definitions_fr/def.alexandria.fr.html", 
                                   "Dictionnaire intégrale", "http://www.memodata.com/alexandria-memodata/definitions_fr/def.di.fr.html",
                                   "Gesamtwoerterbuch",      "http://www.memodata.com/alexandria-memodata/definitions_de/def.di.de.html",
                                   "SEMIOGRAPHE",            "http://www.memodata.com/alexandria-memodata/definitions_fr/def.semiographe.fr.html",
                                   "Sémiographe",            "http://www.memodata.com/alexandria-memodata/definitions_fr/def.semiographe.fr.html"];

// The language used in your html pages 
// ISO 639 alpha-2 code [see 'http://fr.wikipedia.org/wiki/ISO_639' for example] 
// used by Alexandria when it can not determine the "page" language by itself
var WM_Alexandria_defaultSourceLanguage = "fr";

// The language used in your html pages 
// ISO 639 alpha-2 code [see 'http://fr.wikipedia.org/wiki/ISO_639' for example] 
// used by Alexandria when it can not determine the "user" language by itself
// if it is different from WM_Alexandria_defaultSourceLanguage, translations are displayed instead of definitions and synonyms
var WM_Alexandria_defaultTargetLanguage = "fr";

// The charset used in your html pages 
// used by Alexandria when it can not determine it by itself
// e.g. : "ISO-8859-1", "UTF-8", "CP1252", etc.
var WM_Alexandria_defaultCharset        = "UTF-8";

// Is toolbar visible or not ?
// values : Y,N
var WM_Alexandria_show_toolbar = "Y";

// Do you want your reserved words to be double-underlined and reactive ?
// values : Y,N
// default value : N
var WM_Alexandria_highlight_reservedWords = "Y";
// values : 
//  - "closed" : only if the Alexandria window is closed
//  - "always" : no matter if the Alexandria window is closed or open         
// default value : "always"
var WM_Alexandria_highlight_reservedWords_options = "always";
// timeout before reaction of the reserved word (in milliseconds)
var WM_Alexandria_highlight_reservedWords_timeout = "250";
// Highlited reserved words underline style
// values : "double", "dashed", "none"
// default value : double
var WM_Alexandria_highlighted_reservedWords_style = "double";

// ----------------------------------------------------
// Content of the left zone (HTML)
// Only available with the "FRAMES option"
function WM_Alexandria_getLeftZone()
{
  return '';
}
// ----------------------------------------------------
// Content of the right zone (HTML)
// Only available with the "FRAMES option"
function WM_Alexandria_getRightZone()
{
  return '';
}
// ----------------------------------------------------
// Content of the bottom zone (HTML)
// Only available with the "FRAMES option"
function WM_Alexandria_getBottomZone()
{
  return '';
}

// ----------------------------------------------------
// Content of the top zone (HTML)
// Only available with the "FRAMES option"
function WM_Alexandria_getTopZone()
{
  return '';
}



