var now = new Date();
var nextYear = new Date( now.getTime() + 1000 * 60 * 60 * 24 * 365 );
var headerFlashStyle = true;
var swiffyStageLoaded = false;
var hiddenMenuIcon = 'show-header-b.png';
var shownMenuIcon = 'hide-header-b.png';

var fullSizeIcon = 'expand-b.png';
var normalSizeIcon = 'shrink-b.png';

if ( $( '#HeaderButton' ).length ) {
	var imgPath = $( '#HeaderButton img' ).attr( 'src' ).split( '/' );
	imgPath.pop();
	imgPath = imgPath.join( '/' ) + '/';

	var headerState = mw.cookie.get( 'HeaderState' );
	if ( headerState === '1' ) {
		headerFlashStyle = false;
		hideFlashMenu();
	}

	var fullSize = parseInt( mw.cookie.get( 'fullSize' ) );
	if ( fullSize ) {
		setContentFullSize();
	} else {
		setDefaultSize();
	}

	var bodyDefaultFontSize = 14;
	var bodyFontSize = parseInt( mw.cookie.get( 'bodyFontSize' ) );
	var expandedContent = parseInt( mw.cookie.get( 'expandedContent' ) );

	if ( bodyFontSize < 4 ) {
		bodyFontSize = bodyDefaultFontSize;
	}

	if ( bodyFontSize > 40 ) {
		bodyFontSize = 40;
	}

	if ( bodyFontSize != bodyDefaultFontSize ) {
		$( '#bodyContent' ).css( 'font-size', bodyFontSize + 'px' );
	}

	/*** Edit mode - categories ****/
	if ( $( '.categoriesTitle' ).length ) {
		$( '.categoriesTitle' ).click( function() {
			$( '#SelectCategoryList' ).toggle( 'fast', function() {
				if ( $( '.categoriesTitle > div.reversed' ).length ) {
					$( '.categoriesTitle > div' ).removeClass( 'reversed' );
				} else {
					$( '.categoriesTitle > div' ).addClass( 'reversed' );
				}
			} );
		} );
	}
}

function hideFlashMenu() {
	headerFlashStyle = false;

	$( '#HeaderButton img' ).attr( 'src', imgPath + hiddenMenuIcon );
	$( '#HeaderButton img' ).attr( 'alt', $( '#HeaderButton' ).attr( 'data-text2' ) );
	$( '#HeaderButton' ).attr( 'data-original-title', $( '#HeaderButton' ).attr( 'data-text2' ) );

	mw.cookie.set( 'HeaderState', '0', nextYear );

	$( '#swiffycontainer' ).addClass( 'div-hidden' );
	$( '#mobileNav' ).addClass( 'div-visible' );
}

function showFlashMenu() {
	headerFlashStyle = true;

	$( '#HeaderButton' ).attr( 'data-original-title', $( '#HeaderButton' ).attr( 'data-text1' ) );
	$( '#HeaderButton img' ).attr( 'src', imgPath + shownMenuIcon );
	$( '#HeaderButton img' ).attr( 'alt', $( '#HeaderButton' ).attr( 'data-text1' ) );

	mw.cookie.set( 'HeaderState', '1', nextYear );

	$( '#swiffycontainer' ).removeClass( 'div-hidden' );
	$( '#mobileNav' ).removeClass( 'div-visible' );

	if ( !swiffyStageLoaded ) {
		// 2022.05.15 Rififi: fix swiffy not being properly loaded
		mw.loader.using( 'swiffy.fr.js', function() {
			var stage = new swiffy.Stage( document.getElementById( 'swiffycontainer' ), swiffyobject, {} );

			stage.setFlashVars( flashButton );
			stage.start();
			swiffyStageLoaded = true;
		} );
	}
}

function setContentFullSize() {
	fullSize = 1;

	mw.cookie.set( 'fullSize', '1', nextYear );

	$( '#SiteWidth img' ).attr( 'src', imgPath + normalSizeIcon );
	$( '#SiteWidth img' ).attr( 'alt', $( '#SiteWidth' ).attr( 'data-text2' ) );
	$( '#SiteWidth' ).attr( 'data-original-title', $( '#SiteWidth' ).attr( 'data-text2' ) );

	$( '.mw-body' ).addClass( 'mw-body-fluid' );
	$( '.fluid-candidate' ).addClass( 'fluid-row' );
	$( '#fluid-content' ).addClass( 'fluid-content' );
	$( '#fluid-sidebar' ).addClass( 'fluid-sidebar' );
}

function setDefaultSize() {
	fullSize = 0;

	mw.cookie.set( 'fullSize', '0', nextYear );

	$( '#SiteWidth img' ).attr( 'src', imgPath + fullSizeIcon );
	$( '#SiteWidth img' ).attr( 'alt', $( '#SiteWidth' ).attr( 'data-text1' ) );
	$( '#SiteWidth' ).attr( 'data-original-title', $( '#SiteWidth' ).attr( 'data-text1' ) );

	$( '.mw-body' ).removeClass( 'mw-body-fluid' );
	$( '.fluid-candidate' ).removeClass( 'fluid-row' );
	$( '#fluid-content' ).removeClass( 'fluid-content' );
	$( '#fluid-sidebar' ).removeClass( 'fluid-sidebar' );
}

function toggleMenu() {
	if ( headerFlashStyle ) {
		hideFlashMenu();
	} else {
		showFlashMenu();
	}
	$( '#HeaderButton' ).tooltip( 'hide' );
}

function toggleWidth() {
	if ( fullSize != 0 ) {
		setDefaultSize();
	} else {
		setContentFullSize();
	}
	$( '#SiteWidth' ).tooltip( 'hide' );
}

function increaseFont() {
	if ( bodyFontSize > 40 ) {
		return;
	}
	bodyFontSize += 2;
	$( '#bodyContent' ).css( 'font-size', bodyFontSize + 'px' );
	mw.cookie.set( 'bodyFontSize', bodyFontSize, nextYear );
}

function decreaseFont() {
	if ( bodyDefaultFontSize < 6 ) {
		return;
	}
	bodyFontSize -= 2;
	$( '#bodyContent' ).css( 'font-size', bodyFontSize + 'px' );
	mw.cookie.set( 'bodyFontSize', bodyFontSize, nextYear );
}

function defaultFontSize() {
	bodyFontSize = bodyDefaultFontSize;
	$( '#bodyContent' ).css( 'font-size', bodyFontSize + 'px' );
	mw.cookie.set( 'bodyFontSize', bodyFontSize, nextYear );
}

function printPage() {
	window.open(
		mw.util.getUrl( mw.config.get( 'wgPageName' ), { 'printable': 'yes' } ),
		'_self'
	);
}

function inlineIndexTable() {
	/** table to ul & li **/
	if ( $( '.mw-prefixindex-list-table' ).length > 0 ) {
		var indexTable = $( '.mw-prefixindex-list-table' );
		indexTable.after( '<ul class="mw-index-ul"> </ul>' );
		var indexUL = $('.mw-index-ul' );
		$( '.mw-prefixindex-list-table td' ).each( function() {
			indexUL.append( '<li>' + $( this ).html() + '</li>' );
		} );
		indexTable.remove();
	}

	/** page navigation header **/
	if ( $( '#mw-prefixindex-nav-table #mw-prefixindex-nav-form' ).length > 0 ) {
		var indexNav = $( '#mw-prefixindex-nav-form' ).html();
		$( '#mw-prefixindex-nav-form' ).remove();
		$( '#mw-prefixindex-nav-table > tbody > tr:last-child' ).after(
			'<tr><td id="mw-prefixindex-nav-form" class="mw-prefixindex-nav">' + indexNav + '</td></tr>'
		);
	}
}

function initPreferences() {
	var tabsToShow = [ 'mw-prefsection-personal', 'mw-prefsection-rendering' ];
	var fieldsetToHide = [
		'mw-prefsection-personal-i18n',
		'mw-prefsection-rendering-skin',
		'mw-prefsection-rendering-files',
		'mw-prefsection-rendering-diffs',
		'mw-prefsection-rendering-advancedrendering'
	];
	var isFirst = true;

	$( '#mw-input-wpnickname' ).attr( 'size', '20' );
	$( '#mw-input-wprealname' ).attr( 'size', '20' );

	var key;
	for ( key in fieldsetToHide ) {
		$( '#' + key ).hide();
	}

	$( '#mw-input-wplanguage' ).prop( 'disabled', 'disabled' );
	$( '#preferences' ).prepend( '<ul class="nav nav-tabs" role="tablist" id="preferencesList"> </ul>' );
	$( '#preferences > fieldset' ).each( function() {
		if ( tabsToShow.indexOf( $( this ).attr( 'id' ) ) == -1 ) {
			$( this ).addClass( 'tab-pane' );
		} else {
			$( this ).prepend( '<h2>' + $( '#' + $( this ).attr( 'id' ) + ' > legend' ).html() + '</h2>' );
			$( '#' + $( this ).attr( 'id' ) +' > legend' ).remove();
			$( this ).attr( 'role', 'tabpanel' );
			$( this ).addClass( 'tab-pane' );

			if ( isFirst ) {
				isFirst = false;
				$( '#preferencesList' ).append(
					'<li role="presentation" class="active"><a href="#' + $( this ).attr( 'id' ) +
						'" aria-controls="' + $( this ).attr( 'id' ) + '" role="tab" data-toggle="tab">' +
						$( this ).find( 'legend' ).html() + '</a></li>'
				);
				$( this ).addClass( 'active' );
			 } else {
				$( '#preferencesList' ).append(
					'<li role="presentation"><a href="#' + $( this ).attr( 'id' ) +
						'" aria-controls="' + $( this ).attr( 'id' ) + '" role="tab" data-toggle="tab">' +
						$( this ).find( 'legend' ).html() + '</a></li>'
				);
			}
		}
	} );
}

function getArticleLink() {
	var splitHref = $( '.ca-talk a' ).attr( 'href' ).split( ':' );

	return splitHref[0].substring( 0, 19 ) + splitHref[1];
}

function getProfileLink() {
	var splitHref = $( '.ca-talk a' ).attr( 'href' ).split( ':' );

	return mw.util.getUrl( 'User:' + splitHref[1] );
}
