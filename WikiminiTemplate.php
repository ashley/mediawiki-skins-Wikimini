<?php
/**
 * Wikimini based on Vector - Modern version of MonoBook with fresh look and many usability
 * improvements.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Skins
 */

use MediaWiki\MediaWikiServices;

/**
 * QuickTemplate class for Wikimini skin
 * @ingroup Skins
 */
class WikiminiTemplate extends BaseTemplate {
	private $wikiPath = '';
	private $skinPath = '';
	private $imagesPath = '';
	private $forumTag = 'AWCforum';

	/* Functions */

	/**
	 * Outputs the entire contents of the (X)HTML page
	 */
	public function execute() {
		global $wgStylePath, $wgScriptPath;
		global $wgGoogleAnalytics;

		$this->wikiPath = $wgScriptPath;
		$this->skinPath = $wgStylePath . '/Wikimini/';
		$this->imagesPath = $this->skinPath . 'resources/images/';

		$services = MediaWikiServices::getInstance();
		$skin = $this->getSkin();
		$user = $skin->getUser();
		$title = $skin->getRelevantTitle();

		// Build additional attributes for navigation URLs
		$nav = $this->data['content_navigation'];

		if ( $this->config->get( 'WikiminiUseIconWatch' ) ) {
			$mode = $services->getWatchlistManager()->isWatched( $user, $title )
				? 'unwatch'
				: 'watch';

			if ( isset( $nav['actions'][$mode] ) ) {
				$nav['views'][$mode] = $nav['actions'][$mode];
				$nav['views'][$mode]['class'] = rtrim( 'icon ' . $nav['views'][$mode]['class'], ' ' );
				$nav['views'][$mode]['primary'] = true;
				unset( $nav['actions'][$mode] );
			}
		}

		$xmlID = '';

		foreach ( $nav as $section => $links ) {
			foreach ( $links as $key => $link ) {
				if ( $section == 'views' && !( isset( $link['primary'] ) && $link['primary'] ) ) {
					$link['class'] = rtrim( 'collapsible ' . $link['class'], ' ' );
				}

				$xmlID = isset( $link['id'] ) ? $link['id'] : 'ca-' . $xmlID;
				$nav[$section][$key]['attributes'] =
					' id="' . Sanitizer::escapeIdForAttribute( $xmlID ) . '"';
				if ( $link['class'] ) {
					$nav[$section][$key]['attributes'] .= ' class="' . htmlspecialchars( $link['class'] ) . '"';
					unset( $nav[$section][$key]['class'] );
				}
				if ( isset( $link['tooltiponly'] ) && $link['tooltiponly'] ) {
					$nav[$section][$key]['key'] = Linker::tooltip( $xmlID );
				} else {
					$nav[$section][$key]['key'] =
						Xml::expandAttributes( Linker::tooltipAndAccesskeyAttribs( $xmlID ) );
				}
			}
		}

		$this->data['namespace_urls'] = $nav['namespaces'];
		$this->data['view_urls'] = $nav['views'];
		$this->data['action_urls'] = $nav['actions'];
		$this->data['variant_urls'] = $nav['variants'];

		// Reverse horizontally rendered navigation elements
		if ( $this->data['rtl'] ) {
			$this->data['view_urls'] = array_reverse( $this->data['view_urls'] );
			$this->data['namespace_urls'] = array_reverse( $this->data['namespace_urls'] );
			$this->data['personal_urls'] = array_reverse( $this->data['personal_urls'] );
		}

		// Flash button & hide menu condition

		// 20161227/Nixit
		$flashbut = '1'; // default
		$hidemenu1 = '0'; // default

		$isMainPage = $title->isMainPage();

		// @todo FIXME: so ugly :/
		$my_title = $_GET['title'] ?? '';

		if ( $my_title == $this->getMsg( 'mainpage' ) ) {
			// it's the main page
			$hidemenu1 = '1';
		} elseif ( $my_title == $this->getMsg( 'wikimini-link_children_pagename' ) ) {
			// it's the childrens' portal
			$flashbut = '2';
			$hidemenu1 = '1';
		} elseif ( $my_title == $this->getMsg( 'wikimini-link_adults_pagename' ) ) {
			// it's the adults' portal
			$flashbut = '3';
			$hidemenu1 = '1';
		} elseif ( $my_title == $this->getMsg( 'wikimini-link_teachers_pagename' ) ) {
			// it's the teachers' portal
			$flashbut = '4';
			$hidemenu1 = '1';
		} elseif ( substr( $my_title, 9, 8 ) == 'AWCforum' ) {
			// it's the forum
			$flashbut = '5';
			$hidemenu2 = '1';
		} elseif ( $title->isTalkPage() ) {
			// it's a talk page of some kind
			$flashbut = '6';
		}

		// ashley 10 February 2023: rewrote this, but I still don't like this...
		/* also it doesn't want to work as it should, so ":D"
		$childrensPortalTitle = Title::newFromText( $this->msg( 'wikimini-link_children_pagename' ) );
		$adultsPortalTitle = Title::newFromText( $this->msg( 'wikimini-link_adults_pagename' ) );
		$teachersPortalTitle = Title::newFromText( $this->msg( 'wikimini-link_teachers_pagename' ) );
		$isForum = $title->isSpecial( 'AWCforum' );
		// also TODO CHECKME: how do getTitle() and getRelevantTitle() (the $title var here) differ in practise?
		if ( $isMainPage ) {
			// it's the main page
			$hidemenu1 = '1';
		} elseif ( $title->isTalkPage() ) {
			// it's a talk page of some kind
			$flashbut = '6';
		} elseif ( $isForum ) {
			// it's the forum
			$flashbut = '5';
			$hidemenu2 = '1';
		} elseif ( $title->equals( $childrensPortalTitle ) ) {
			// it's the childrens' portal
			$flashbut = '2';
			$hidemenu1 = '1';
		} elseif ( $title->equals( $adultsPortalTitle ) ) {
			// it's the adults' portal
			$flashbut = '3';
			$hidemenu1 = '1';
		} elseif ( $title->equals( $teachersPortalTitle ) ) {
			// it's the teachers' portal
			$flashbut = '4';
			$hidemenu1 = '1';
		}
		*/

		/* 20161227/Nixit
		else
		{
			$flashbut = "1";
		}
		*/

		$this->data['pageLanguage'] = $skin->getTitle()->getPageViewLanguage()->getHtmlCode();

		// Output HTML Page
?>
	<script>
		var flashButton ='<?php echo "button=$flashbut" ?>';
	</script>
	<!-- modal -->
	<div class="modal" id="uploadModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					 <h4 class="modal-title">Upload</h4>
				</div>
				<div class="modal-body">
					<iframe id="stockIframe" data-src="<?php echo htmlspecialchars( SpecialPage::getTitleFor( 'Upload' )->getFullURL() /* @todo FIXME $this->data['nav_urls']['upload']['href']*/ ) ?>" src="" width="770" height="500"></iframe>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<div class="mw-body container" role="main">
	<?php if ( !$this->data['printable'] ) { ?>
	<?php echo $this->renderMobileSearchBox(); ?>
		<div class="row hidden-xs">
			<div id="swiffycontainer" style="width: 760px; height: 278px">
			</div>
		</div>
		<div class="row visible-xs-block" id="mobileNav">
			<nav class="navbar navbar-default navbar-wikimini">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav" aria-expanded="false">
							<span class="sr-only"><?php echo $this->msg( 'wikimini-toggle-navigation' ) ?></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a href="<?php echo htmlspecialchars( $this->data['nav_urls']['mainpage']['href'] ) ?>" class="navbar-brand">
							<img src="<?php $this->text( 'logopath' ) ?>" alt="<?php $this->text( 'sitename' ) ?>"/>
						</a>
					</div>

					<!-- Nav -->
					<div class="collapse navbar-collapse" id="mainNav">
						<ul class="nav navbar-nav">
							<!--
								@todo FIXME: The way the links are generated below isn't portable at all...
							-->
							<li class="active wm-home">
								<a href="/" title="<?php echo $this->msg( 'wikimini-link_home_title' ); ?>">
									<?php echo $this->msg( 'wikimini-link_home_text' ); ?>
								</a>
							</li>
							<li class="wm-children">
								<a href="/wiki/<?php echo $this->msg( 'wikimini-link_children_pagename' ); ?>" title="<?php echo $this->msg( 'wikimini-link_children_title' ); ?>">
									<?php echo $this->msg( 'wikimini-link_children_text' ); ?>
								</a>
							</li>
							<li class="wm-adults">
								<a href="/wiki/<?php echo $this->msg( 'wikimini-link_adults_pagename' ); ?>" title="<?php echo $this->msg( 'wikimini-link_adults_title' ); ?>">
									<?php echo $this->msg( 'wikimini-link_adults_text' ); ?>
								</a>
							</li>
							<li class="wm-teachers">
								<a href="/wiki/<?php echo $this->msg( 'wikimini-link_teachers_pagename' ); ?>" title="<?php echo $this->msg( 'wikimini-link_teachers_title' ); ?>">
									<?php echo $this->msg( 'wikimini-link_teachers_text' ); ?>
								</a>
							</li>
							<li class="wm-forum">
								<a href="/wiki/Special:AWCforum" title="<?php echo $this->msg( 'wikimini-link_forum_title' ); ?>">
									<?php echo $this->msg( 'wikimini-link_forum_text' ); ?>
								</a>
							</li>
							<li class="wm-help">
								<a href="/wiki/<?php echo $this->msg( 'wikimini-link_help_pagename' ); ?>" title="<?php echo $this->msg( 'wikimini-link_help_title' ); ?>">
									<?php echo $this->msg( 'wikimini-link_help_text' ); ?>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>

		<div class="row">
		<?php
		if (
			$this->data['sitenotice'] &&
			// !$title->isSubpageOf( Title::newMainPage() )
			strcmp( $my_title, substr( $this->data['nav_urls']['mainpage']['href'], strrpos( $this->data['nav_urls']['mainpage']['href'], '/' ) + 1 ) ) != 0
		) {
		?>
			<div id="siteNotice"><?php $this->html( 'sitenotice' ) ?></div>
		<?php
		}

		echo $this->getIndicators();
		?>
		</div>
		<div class="row">
			<?php
			if ( $hidemenu1 <> '1' ) {
				$this->renderMobileOptionbox();
			}
			?>
		</div>

	<?php } ?>
		<div id="fluid-row" class="row fluid-candidate">
			<?php if ( !$this->data['printable'] && substr_count( strtolower( $my_title ), strtolower( $this->forumTag ) ) < 1 /* $isForum */ ) { ?>
			<div id="fluid-content" class="col-md-7">
			<?php } else { ?>
			<div class="col-md-12 col-lg-12">
			<?php } ?>
			<div id="content">
				<h1 id="firstHeading" class="firstHeading" lang="<?php $this->text( 'pageLanguage' ); ?>">
				<?php
				if ( $isMainPage ) {
					 echo $this->msg( 'wikimini-home_title' );
				} else {
					// Need always html() instead of text() b/c of the title being wrapped in a <span> in 1.39+
					$this->html( 'title' );
				}
				?>
			</h1>

			<?php $this->html( 'prebodyhtml' ) ?>

			<div id="bodyContent" class="mw-body-content">
				<?php
				if ( $this->data['isarticle'] && $hidemenu1 <> '1' ) {
					?>
					<div id="siteSub"><?php $this->msg( 'tagline' ) ?></div>
				<?php
				}
				?>
				<div id="contentSub"<?php
				$this->html( 'userlangattributes' )
				?>><?php $this->html( 'subtitle' ) ?></div>
				<?php
				if ( $this->data['undelete'] ) {
					?>
					<div id="contentSub2"><?php $this->html( 'undelete' ) ?></div>
				<?php
				}
				?>
				<?php
				if ( $this->data['newtalk'] ) {
					?>
					<div class="usermessage"><?php $this->html( 'newtalk' ) ?></div>
				<?php
				}
				?>
				<div id="jump-to-nav"></div>
				<a class="mw-jump-link" href="#content"><?php $this->msg( 'wikimini-jump-to-navigation' ) ?></a>
				<a class="mw-jump-link" href="#searchInput"><?php $this->msg( 'wikimini-jump-to-search' ) ?></a>
				<?php $this->html( 'bodycontent' ) ?>
				<?php
					if ( $this->data['catlinks'] ) {
						$this->html( 'catlinks' );
					}
				?><!-- end content -->

				<?php
				if ( $this->data['dataAfterContent'] ) {
					$this->html( 'dataAfterContent' );
				}
				?>
				</div>
			</div>

			</div>

			<?php if ( !$this->data['printable'] && substr_count( strtolower( $my_title ?? '' ), strtolower( $this->forumTag ) ) < 1 /* $isForum */) { ?>
			<div id="bodyTools" class="col-md-5 text-center fluid-sidebar">

			<!-- Options (print, home, etc.)  -->
				<div class="OptionsBox">
					<div class="visualClear"><span id="OptionsTexte" class="OptionsTexte_Hidden"></span></div>
					<div id="MenuOptions">
					<!--   <?php //$this->msg( 'tagline' ) ?>-->
						<a id="HeaderButton" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-reduce-header' ) ?>" data-text1="<?php $this->msg( 'wikimini-tooltip-reduce-header' ) ?>"  data-text2="<?php $this->msg( 'wikimini-tooltip-show-header' ) ?>">
							<img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>hide-header-b.png" alt="<?php $this->msg( 'wikimini-tooltip-reduce-header' ) ?>">
						</a>

						<img src="<?php echo $this->imagesPath; ?>blank-18x18.png" width="9" alt="" />

						<a id="SiteWidth" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-wide-site' ) ?>" data-text1="<?php $this->msg( 'wikimini-tooltip-wide-site' ) ?>" data-text2="<?php $this->msg( 'wikimini-tooltip-narrow-site' ) ?>">
							<img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>expand-b.png" alt="<?php $this->msg( 'wikimini-tooltip-wide-site' ) ?>">
						</a>

						<img src="<?php echo $this->imagesPath; ?>blank-18x18.png" width="9" alt="" />

						<a id="IncreaseFont" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-increase-font' ) ?>">
							<img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>font-plus-b.png" alt="<?php $this->msg( 'wikimini-tooltip-increase-font' ) ?>">
						</a>
						<a id="DecreaseFont" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-reduce-font' ) ?>">
							<img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>font-minus-b.png" alt="<?php $this->msg( 'wikimini-tooltip-reduce-font' ) ?>">
						</a>
						<a id="DefaultFontSize" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-reset-font' ) ?>">
							<img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>font-default-b.png" alt="<?php $this->msg( 'wikimini-tooltip-reset-font' ) ?>">
						</a>

						<img src="<?php echo $this->imagesPath; ?>blank-18x18.png" width="9" alt="" />

						<a id="PrintVersion" data-toggle="tooltip" data-original-title="<?php $this->msg( 'wikimini-tooltip-print-version' ) ?>">
							<img class="MenuOptionsBouton" src="<?php echo $this->imagesPath; ?>printer-b.png" alt="<?php $this->msg( 'wikimini-tooltip-print-version' ) ?>">
						</a>
					</div>
				</div>

			<?php
			if ( $hidemenu1 <> '1' ) {
				$this->renderOptionBox();
			}

			// I'm hiding all the menus on special pages as some equire more space (galleries, for example)
			if ( $this->data['notspecialpage'] ) { ?>
			<!-- search -->
			<?php $this->renderSearchBox(); ?>

			<!-- Daily Picture -->
			<?php
			// Display only if the main page is displayed
			if ( $isMainPage ) {
				$this->renderDailyPicture();
			}
			?>

			<!-- Toolbox -->
			<?php
			$this->renderToolbox();
			?>
			<!-- alert - abuse -->
				<p style="text-align:center;">
					<a href="/wiki/<?php $this->msg( 'wikimini-link_abuse_pagename' ); ?>" title="<?php echo $this->msg( 'wikimini-link_abuse_title' ); ?>" target="_blank"><img src="<?php echo $this->imagesPath; ?>wikiboo13_36x36_trans.png" alt="Wikiboo" /><br /><?php echo $this->msg( 'wikimini-link_abuse_text' ); ?></a>
					</p>
		<?php } // end of if notspecialpage ?>
			</div>
			<?php } ?>
		</div>

		<?php if ( !$this->data['printable'] ) { ?>
		<div class="row fluid-candidate">
			<div class="content-bottom">
				<h5><img src="<?php echo $this->imagesPath; ?>wikiboo12_36x36_trans.png" alt="Wikiboo" />&nbsp;<?php $this->msg( 'personaltools' ) ?></h5>
				<div class="box-bottom">
					<div class="personal-tools">
						<ul>
						<?php
							foreach ( $this->data['personal_urls'] as $key => $item ) {
								echo $skin->makeListItem( $key, $item );
							}
						?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>

		<div class="row fluid-candidate">
			<!--footer -->
			<div class="col-md-12 text-center" id="footer">
				<ul id="f-list">
					<?php
					$footerlinks = [
						'viewcount', 'lastmod', 'numberofwatchingusers', 'credits', 'copyright',
						'about', 'disclaimer', 'privacy', 'tagline',
					];
					foreach ( $footerlinks as $aLink ) {
						if ( isset( $this->data[$aLink] ) && $this->data[$aLink] ) {
				?>			<li id="<?php echo $aLink ?>"><?php $this->html( $aLink ) ?></li>
				<?php 	}
					}

					// str_replace() hack makes sure that the list items are "joined" with the existing,
					// open unordered list, instead of letting MW think we want to start a *new* list here!
					echo str_replace( [ '<ul>', '</ul>' ], '', $this->getMsg( 'wikimini-additional_footer_links' )->parse() );
					?>
				</ul>
				<p class="center"><?php echo $this->getMsg( 'wikimini-footer_title' )->parse(); ?>
					&nbsp;|&nbsp;
					<!-- @todo FIXME: the inline French alt titles should be made i18n-able -->
					<a rel="nofollow" href="https://validator.w3.org/check/referer">
						<img src="<?php echo $this->imagesPath; ?>validator-xhtml.gif" alt="Wiki valide XHTML 1.0" />
					</a>&nbsp;
					<a rel="nofollow" href="https://jigsaw.w3.org/css-validator/check/referer?profile=css3">
						<img src="<?php echo $this->imagesPath; ?>validator-css.gif" alt="Wiki valide CSS 3" />
					</a>&nbsp;
					<a rel="nofollow" href="https://www.w3.org/WAI/WCAG1A-Conformance">
						<img src="<?php echo $this->imagesPath; ?>validator-aaa.gif" alt="Wiki valide WCAG1A" />
					</a>&nbsp;
					<!-- @todo FIXME: should use Title or something, but it doesn't want to play nice... -ashley, 10 February 2023 -->
					<a href="/wiki/<?php echo $this->getMsg( 'wikimini-link_syndication_pagename' )->escaped(); ?>">
						<img src="<?php echo $this->imagesPath; ?>rss-xml.gif" alt="<?php echo $this->getMsg( 'wikimini-link_syndication_title' )->escaped(); ?>" />
					</a>
				</p>
			</div>
		<!--footer end-->

		</div>
	</div>

<?php $this->html( 'reporttime' ) ?>
<?php if ( $this->data['debug'] ): ?>
<!-- Debug output:
<?php $this->text( 'debug' ); ?>

-->
<?php endif;

		/************ GOOGLE ANALYTICS ***********/
		if ( strlen( $wgGoogleAnalytics ) ) {
?>
<!--
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<?php echo $wgGoogleAnalytics; ?>']);
		_gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
-->
<?php
		}
		/************ END OF GOOGLE ANALYTICS ***********/
	}

	protected function renderOptionBox() {
		$skin = $this->getSkin();
		$title = $skin->getTitle();
		$user = $skin->getUser();
 ?>
								<div class="option-box text-center">
									<div class="text-left">
										<h5><img src="<?php echo $this->imagesPath; ?>wikiboo06_36x36_trans.png" alt="Wikiboo" /> <?php $this->msg( 'views' ) ?></h5>
										<br />
										<div class="content">
											<ul>
												<?php
												$list = [
													'nstab-main' => 1,
													'nstab-user' => 1,
													'talk' => 1,
													've-edit' => 1,
													'edit' => 1,
													'move' => 1,
													'history' => 1,
													'watch' => 1,
													'unwatch' => 1
												];

												/** edit/ve-edit actions only for articles if user not logged in **/
												if ( !$user->isRegistered() && !$this->data['isarticle'] ) {
													unset( $list['ve-edit'] );
													unset( $list['edit'] );
												} else {
													// edit link for anonymous users
													// only if the page is not in edit mode and if it is an article that exists

													/* 20161230/Nixit
													if (!$_GET['action']=='edit' && !$_GET['veaction']=='edit' && $this->data['isarticle'] &&$this->data['articleid'] > 0){
													*/

													if (
														( array_key_exists( 'action', $_GET ) && !$_GET['action'] == 'edit' ) &&
														( array_key_exists( 'veaction', $_GET ) && !$_GET['veaction'] == 'edit' ) &&
														// ( array_key_exists( 'isarticle', $_GET ) && $this->data['isarticle'] ) &&
														$this->data['articleid'] > 0
													) {
														// end
														$anonEdit['class'] = '';
														$anonEdit['text'] = $this->getMsg( 'edit' );
														$anonEdit['href'] = $title->getFullURL( [ 'action' => 'edit' ] );
														$anonEdit['primary'] = 1;
														$anonEdit['id'] = 'ca-edit';
														$this->data['content_actions']['edit'] = $anonEdit;
													}
												}

												if ( $title->inNamespace( NS_CATEGORY ) ) {
													unset( $list['ve-edit'] );
												}

												/** patch for sysop **/
												if ( $user->isAllowed( 'protect' ) ) {
													$list['protect'] = 1;
												}
												if ( $user->isAllowed( 'delete' ) ) {
													$list['delete'] = 1;
												}

												foreach ( $this->data['content_actions'] as $key => $tab ) {
													if ( !isset( $list[$key] ) ) {
														continue;
													}

													if ( strcmp( $key, 'move' ) == 0 && isset( $this->data['namespace_urls']['user'] ) ) {
														continue;
													}

													// This would otherwise do, but it generates *ID*s for the
													// elements (e.g. #ca-edit) but we want *classes* (e.g. .ca-edit)
													// Another option would be to change the CSS, eh.
													// echo $skin->makeListItem( $key, $tab );
													?>
													<li class="ca-<?php
														echo Sanitizer::escapeIdForAttribute( $key );
														if ( $tab['class'] ) {
															echo ' ' . htmlspecialchars( $tab['class'] );
														}
														?>"<?php
														// Stupid hack for VisualEditor
														if ( $key === 'edit' || $key === 've-edit' ) {?> id="ca-<?php echo $key ?>"<?php } ?> title="<?php echo Linker::titleAttrib( 'ca-' . $key, 'withaccess' ) ?>" accesskey="<?php echo Linker::accesskey( 'ca-' . $key ) ?>"><a href="<?php echo htmlspecialchars( $tab['href'] ) ?>"><?php
																 echo htmlspecialchars( $tab['text'] ) ?></a>
													</li>
												<?php
													// @todo FIXME: the classify URL construction below just isn't guaranteed to be 100% right all the time, I think
													// --ashley, 10 February 2023
													if ( strcmp( $key, 'edit' ) == 0 ) {
															?>
																<li class="ca-<?php
																	echo Sanitizer::escapeIdForAttribute( $key );
																	if ( $tab['class'] ) {
																		echo ' ' . htmlspecialchars( $tab['class'] );
																	}
																	?>" id="classify">
																	<a href="<?php echo htmlspecialchars( $tab['href'] ) ?>&classify=1">
																			<?php echo $this->getMsg( 'wikimini-link_classify_page' ); //htmlspecialchars($tab['text'])
																 ?>
																	</a>
																</li>
												<?php
													}
												}
												?>
												</ul>
												<p class="p1">
													<img src="<?php echo $this->imagesPath; ?>home.png" alt="<?php $this->msg( 'tooltip-p-logo' ) ?>" />&nbsp;&nbsp;
														<a class="blackbold" href="<?php echo $this->wikiPath; ?>" title="<?php $this->msg( 'tooltip-n-mainpage' ) ?> [z]" accesskey="z"><?php $this->msg( 'tooltip-p-logo' ) ?></a>
												</p>
										</div>
									</div>
								</div>
<?php
	}

	protected function getRandomPictureKids() {
		// Random Image
		$total = '116';

		// Change to the type of files to use eg. .jpg or .gif
		$file_type = '.jpg';
		$file_prefix = 'wikimini-kids-';

		// Change to the location of the folder containing the images
		$image_folder = $this->imagesPath . 'kids';

		// You do not need to edit below this line
		$start = '1';
		$random = mt_rand( $start, $total );
		$image_name = "$file_prefix$random$file_type";

		$tagline = $this->getMsg( 'tagline' )->escaped();
		return "<img src=\"$image_folder/$image_name\" alt=\"$tagline\" />";
	}

	protected function renderMobileOptionbox() {
		$user = $this->getSkin()->getUser();
		?>
		<div class="mobile-tool-box text-center">
			<div>
				<div class="dropdown">
					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<?php $this->msg( 'views' ) ?>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<?php
						$list = [
							'talk' => 1,
							've-edit' => 1,
							'edit' => 1,
							'move' => 1,
							'history' => 1,
							'watch' => 1,
							'unwatch' => 1
						];

						/** edit/ve-edit actions only for articles if user not logged in **/
						if ( !$user->isRegistered() && !$this->data['isarticle'] ) {
							unset( $list['ve-edit'] );
							unset( $list['edit'] );
						}

						/** patch for sysop **/
						if ( $user->isAllowed( 'protect' ) ) {
							$list['protect'] = 1;
						}
						if ( $user->isAllowed( 'delete' ) ) {
							$list['delete'] = 1;
						}

						foreach ( $this->data['content_actions'] as $key => $tab ) {
							if ( !isset( $list[$key] ) ) {
								continue;
							}

							if ( strcmp( $key, 'move' ) == 0 && isset( $this->data['namespace_urls']['user'] ) ) {
								continue;
							}
						?>
						<li class="ca-<?php
							// This is kinda icky, but...by default the elements get a "ca-<key>" class, like
							// "ca-edit", "ca-history", and so on, but apparently the "ca-edit" element
							// can also receive the "collapsible" class, and in that case, we should
							// output a space-delimited list of classes instead of outputting the
							// "class" attribute twice, as this code used to do.
							// --ashley, 8 February 2023
							echo Sanitizer::escapeIdForAttribute( $key );
							if ( $tab['class'] ) {
								echo ' ' . htmlspecialchars( $tab['class'] );
							}
							?>" title="<?php echo Linker::titleAttrib( 'ca-' . $key, 'withaccess' ) ?>" accesskey="<?php echo Linker::accesskey( 'ca-' . $key ) ?>"><a href="<?php echo htmlspecialchars( $tab['href'] ); ?>"><?php
							 echo htmlspecialchars( $tab['text'] ) ?></a></li>
						<?php
						}
						?>
						<li class="ca-home">
							<a href="/" title="<?php $this->msg( 'tooltip-n-mainpage' ) ?> [z]" accesskey="z"><?php $this->msg( 'tooltip-p-logo' ) ?></a>
						</li>
					  </ul>
				</div>
			</div>
		</div>
<?php
	}

	protected function renderToolbox() {
		global $wgUserLevels;
		?>
		<div class="tool-box text-center">
			<div class="text-left">
				<h5>
					<img src="<?php echo $this->imagesPath; ?>wikiboo11_36x36_trans.png" alt="Wikiboo" />
					<?php $this->msg( 'toolbox' ) ?>
				</h5>
			</div>
				<?php
					echo $this->getRandomPictureKids();
					// @todo FIXME: the "new article" link construction below is ugly and fragile
					// It should use Title instead for building the link. --ashley, 10 February 2023
				?>

			<div class="tool-box-content text-left">
				<div class="text-left">
					<ul>
						<li id="t-newarticle">
							<a href="/wiki/<?php echo $this->msg( 'wikimini-link_new_article_pagename' ); ?>" title="<?php echo $this->msg( 'wikimini-link_new_article' ); ?>"><?php echo $this->msg( 'wikimini-link_new_article' ); ?></a>
						</li>
				<?php
					if ( $this->data['nav_urls']['upload'] ) { ?>
						<li id="t-upload">
							<a id="uploadBtn"><?php $this->msg( 'upload' ) ?></a>
						</li>
					<?php
					}
					?>
				<?php
				// @todo FIXME/CHECKME: try out $this->data['sidebar']['TOOLBOX'] here --ashley, 10 February 2023
				foreach ( [ 'contributions', 'log', 'blockip', 'emailuser', 'specialpages' ] as $special ) {
						if ( $this->data['nav_urls'][$special] ) { ?>
						<li id="t-<?php echo $special ?>">
							<a href="<?php echo htmlspecialchars( $this->data['nav_urls'][$special]['href'] ) ?>" title="<?php echo Linker::titleAttrib( 't-' . $special, 'withaccess' ) ?>" accesskey="<?php echo Linker::accesskey( 't-' . $special ) ?>">
								<?php $this->msg( $special ) ?>
							</a>
						</li>
					<?php 	}
					}
					if ( $this->data['notspecialpage'] ) { ?>
						<li id="t-whatlinkshere"><a href="<?php
							echo htmlspecialchars( $this->data['nav_urls']['whatlinkshere']['href'] )
							?>" title="<?php echo Linker::titleAttrib( 't-whatlinkshere', 'withaccess' ) ?>" accesskey="<?php echo Linker::accesskey( 't-whatlinkshere' ) ?>"><?php $this->msg( 'whatlinkshere' ) ?></a></li>
					<?php
					}
					if ( $this->data['feeds'] ) { ?>
						<li id="feedlinks"><?php
						foreach ( $this->data['feeds'] as $key => $feed ) {
								?><span id="feed-<?php echo Sanitizer::escapeIdForAttribute( $key ) ?>"><a href="<?php
								echo htmlspecialchars( $feed['href'] ) ?>"><?php echo htmlspecialchars( $feed['text'] ) ?></a>&nbsp;</span>
								<?php
						} ?></li><?php
					}
				?>
				</ul>
					<p class="p1">
						<?php
						// Wiki Champions link, either to SocialProfile's Special:TopUsers
						// or then to Special:ContributionScores (its own, separate, unrelated)
						// extension, depending on which one is installed
						if ( class_exists( 'TopUsersPoints' ) && is_array( $wgUserLevels ) ) {
							$wikiChampionsPage = SpecialPage::getTitleFor( 'TopUsers' );
						} else {
							$wikiChampionsPage = SpecialPage::getTitleFor( 'ContributionScores' );
						}
						?>
						<img src="<?php echo $this->imagesPath; ?>award_star_gold_1.png" alt="<?php echo $this->msg( 'wikimini-link_wikichampions' ); ?>" />&nbsp;&nbsp;<a class="blackbold" href="<?php echo $wikiChampionsPage->getFullURL(); ?>" title="<?php echo $this->msg( 'wikimini-link_wikichampions' ); ?>"><?php echo $this->msg( 'wikimini-link_wikichampions' ); ?></a><br />
						<img src="<?php echo $this->imagesPath; ?>bell.png" alt="<?php $this->msg( 'wantedpages' ) ?>" />&nbsp;&nbsp;<a class="blackbold" href="<?php echo SpecialPage::getTitleFor( 'Wantedpages' )->getFullURL(); ?>" title="<?php $this->msg( 'wantedpages' ) ?>"><?php $this->msg( 'wantedpages' ) ?></a><br />
						<img src="<?php echo $this->imagesPath; ?>clock.png" alt="<?php $this->msg( 'recentchanges' ) ?>" />&nbsp;&nbsp;<a class="blackbold" href="<?php echo SpecialPage::getTitleFor( 'Recentchanges' )->getFullURL(); ?>" title="<?php $this->msg( 'tooltip-n-recentchanges') ?>"><?php $this->msg( 'recentchanges' ) ?></a><br />
					</p>
				</div>
			</div>
		</div>
<?php
	}

	// @todo FIXME: sure would be lovely if this were more configurable via LocalSettings.php instead --ashley, 10 February 2023
	protected function renderDailyPicture() {
?>
		<div class="dailyPic">
			<img src="<?php echo $this->imagesPath; ?>Wikimini-daily_picture-<?php $this->text( 'lang' ) ?>.png" alt="<?php echo $this->msg( 'wikimini-picture_of_the_day' ); ?>" style="-moz-transform:rotate(2deg); -webkit-transform:rotate(2deg);" />
			<div id="multicouche">
				<?php
				if ( isset( $_SERVER['HTTP_HOST'] ) ) {
					if ( defined( 'WIKIMINI_PROJECT_UID' ) && WIKIMINI_PROJECT_UID === 'sv' ) {
						include_once 'pictures-of-the-day/sv/picture-of-the-day.html';
					} elseif ( defined( 'WIKIMINI_PROJECT_UID' ) && WIKIMINI_PROJECT_UID === 'fr' ) {
						include_once 'pictures-of-the-day/fr/picture-of-the-day.html';
					}
				}
				?>
			</div>
		</div>
<?php
	}

	protected function renderSearchBox() {
?>
				<div id="p-search" role="search" class="search-box text-left">
					<div class="search-logo">
						<img src="<?php echo $this->imagesPath; ?>wikiboo08_36x36_trans.png" alt="Wikiboo" />
					</div>
					<div class="search-content">
						<h5<?php $this->html( 'userlangattributes' ) ?>>
							<?php $this->msg( 'search' ) ?>
						</h5>
						<form action="<?php $this->text( 'wgScript' ) ?>" id="searchform">
							<?php
							if ( $this->getSkin()->getConfig()->get( 'WikiminiUseSimpleSearch' ) ) {
							?>
							<div id="simpleSearch">
								<?php
							} else {
							?>
								<div>
									<?php
							}

							echo $this->makeSearchInput( [ 'id' => 'searchInput' ] );
							echo Html::hidden( 'title', $this->get( 'searchtitle' ) );
							echo $this->makeSearchButton(
								'fulltext',
								[ 'id' => 'mw-searchButton', 'class' => 'searchButton mw-fallbackSearchButton' ]
							);
							echo $this->makeSearchButton(
								'go',
								[ 'id' => 'searchButton', 'class' => 'searchButton' ]
							);
							?>
							</div>
						</form>
					</div>
				</div>
		<?php
	}

	protected function renderMobileSearchBox() {
		$startDiv = $this->getSkin()->getConfig()->get( 'WikiminiUseSimpleSearch' ) ? '<div id="simpleSearch">' : '<div>';
		?>
						<div id="mobile-p-search" role="search" class="mobile-search-box text-left">
							<div class="search-content">
								<form action="<?php $this->text( 'wgScript' ) ?>" id="searchform">
									<?php
									echo $startDiv;

									echo $this->makeSearchInput( [ 'id' => 'searchInput' ] );
									echo Html::hidden( 'title', $this->get( 'searchtitle' ) );
									echo $this->makeSearchButton(
										'fulltext',
										[ 'id' => 'mw-searchButton', 'class' => 'searchButton mw-fallbackSearchButton' ]
									);
									echo $this->makeSearchButton(
										'go',
										[ 'id' => 'searchButton', 'class' => 'searchButton' ]
									);
									?>
									</div>
								</form>
							</div>
						</div>
<?php
	}

}
